/*
  Задание: написать функцию, для глубокой заморозки обьектов.

  Обьект для работы:
  let universe = {
    infinity: Infinity,
    good: ['cats', 'love', 'rock-n-roll'],
    evil: {
      bonuses: ['cookies', 'great look']
    }
  };

  frozenUniverse.evil.humans = []; -> Не должен отработать.

  Методы для работы:
  1. Object.getOwnPropertyNames(obj);
      -> Получаем имена свойств из объекта obj в виде массива

  2. Проверка через typeof на обьект или !== null
  if (typeof prop == 'object' && prop !== null){...}

  Тестирование:

  let FarGalaxy = DeepFreeze(universe);
      FarGalaxy.good.push('javascript'); // false
      FarGalaxy.something = 'Wow!'; // false
      FarGalaxy.evil.humans = [];   // false

*/

let universe = {
  infinity: Infinity,
  good: ['cats', 'love', 'rock-n-roll'],
  evil: {
    bonuses: ['cookies', 'great look']
  },
  obj: {
    obj: {
      obj: {
        obj: {
          cat: 'meow'
        }
      }
    }
  }
};


const DeepFreeze = ( obj ) => {
  if( typeof( obj ) === 'object' ){
    let allKey = Object.getOwnPropertyNames( obj );
      allKey.forEach( ( item ) => {
          if ( typeof obj[item] === 'object' && obj[item] !== null ){
            DeepFreeze( obj[item] );
          }
      });
      console.log('frozet obj:', obj )
      return Object.freeze( obj );
  }
}

const work = ( obj ) => {

   let FarGalaxy = DeepFreeze(universe);
      // FarGalaxy.good.push('javascript'); // false
      // FarGalaxy.something = 'Wow!'; // false
      // FarGalaxy.evil.humans = [];   // false
      // FarGalaxy.obj.obj.obj.obj.cat = 'woof';
      console.log( FarGalaxy );

}

export default work;
